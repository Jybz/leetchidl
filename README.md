
./leetchi.sh --url=[...] --get=[...]
--url=[...]    : The URL like www.leetchi.com/c/cagnotte
--get=[...]    : Which information to recover, like
               : =informations    : print a message with informations.
               : =donors          : print a list of donors.
               : =comments        : print a list of comments.
--help         : This message.
--output=[...] : redirecting output.


generateStat.sh --input=[...] --output_prefix=[...]
--input=[...]         : the input file to analyse, given by leetchi.sh.
--output_prefix=[...] : the prefix for output files.
--help                : this help.
