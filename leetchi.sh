#!/bin/sh +x


################################
#      FUNCTIONS PART          #
################################

PrintHelp()
{
    echo -e "${0} --url=[...] --get=[...]" >&2
    echo -e "--url=[...]\t: The URL like www.leetchi.com/c/cagnotte" >&2
    echo -e "--get=[...]\t: Which information to recover, like" >&2
    echo -e "\t\t: =informations\t: print a message with informations." >&2
    echo -e "\t\t: =donors\t: print a list of donors." >&2
    echo -e "\t\t: =comments\t: print a list of comments." >&2
    echo -e "--help\t: This message." >&2
    echo -e "--output=[...]\t: redirecting output." >&2
}

#Function not implemented yet.
checkURL()
{
    echo "1"
}

#Function which gives back a part of a HTML page.
getPart()
{
    part=${1}
    file=${2}
    echo $(sed -n '/<'"${part}"'/,/<\/'"${part}"'/p' ${file})
}

################################
#      START CODE PART         #
################################

#If no arguments, print help
if [ $# == 0 ]; then
    PrintHelp
    exit
fi

hasURL=0
hasGet=0
hasHelp=0
hasOut=0

#echo "Check arguments" >&2
for args in ${*}; do
    
    ## Check if query for help
    if [ ${args} == "--help" ]; then
        hasHelp=1
    ##Check all options
    #Case --url=
    elif [ 1 -eq $( echo "${args}" | grep -c "url=") ] ; then
        predicat=$( echo "${args}" | sed -e "s/^\(--url=\)\(.*\)$/\1/" )
        URL=$( echo "${args}" | sed -e "s/^\(--url=\)\(.*\)$/\2/" )
        if [ ${predicat} == "--url=" ] && ! [ -z ${URL} ] ; then
            hasURL=$(checkURL ${URL})
        fi
    #case --output=
    elif [ 1 -eq $( echo "${args}" | grep -c "output=") ] ; then
        outFile=$( echo "${args}" | sed -e "s/^\(--output=\)\(.*\)$/\2/" )
        if [ -e ${outFile} ]; then
            if [ -d ${outFile} ]; then
                echo "The output is a directory !" >&2
                hasOut=0
            elif [ -f ${outFile} ]; then
                hasOut=1
            fi
        else
            touch ${outFile}
            hasOut=1
        fi
    #case --get=
    elif [ 1 -eq $( echo "${args}" | grep -c "get=") ] ; then
        get=$( echo "${args}" | sed -e "s/^\(--get=\)\(.*\)$/\2/" )
        case ${get} in
            "informations")
                #echo "got information" >&2
                hasGet=1
                ;;
            "donors")
                #echo "got donors" >&2
                hasGet=1
                ;;
            "comments")
                #echo "got comments" >&2
                hasGet=1
                ;;
            *)
                ;;
        esac
    fi
done

if [ 1 -eq ${hasHelp} ]; then
    PrintHelp
    exit
elif [ 0 -eq ${hasGet} ]; then
    echo "Need get argument." >&2
    exit
elif [ 0 -eq ${hasURL} ]; then
    echo "Need URL." >&2
    exit
fi


URL=$(echo ${URL} | sed "s/^https\?:\/\/[w\.]\+//")
domain=$(echo ${URL} | sed "s/^\([[:alpha:].]*\)\/\(.*\)/\1/" )
  rest=$(echo ${URL} | sed "s/^\([[:alpha:].]*\)\/\(.*\)/\2/" )
urlbase="www.${domain}"

#make some temporary variable which will remains in the RAM.
myCookie=$(mktemp)
oneHTML=$(mktemp)
tmp1=$(mktemp)
tmp2=$(mktemp)

if [ 1 -eq ${hasOut} ]; then
    output=$(echo "${outFile}")
    else
    output="/dev/stdout"
fi
#output="/dev/null"
#output="/dev/stdin"
#output="./output.html"
precision=15
precisionFine=$(echo ${precision}+2| bc)

echo "Check anti-DDoS system" >&2
#Getting the Cookie and bypassing the antiDDoS
dryRun=0
debugFile=./debug.html
if [ 1 -eq "${dryRun}" ]; then
    if [ -a ${debugFile} ]; then
        cat ${debugFile} > ${oneHTML}
    else
        curl -c myCookies https://www.leetchi.com/ -o ${debugFile}
        cat ${debugFile} > ${oneHTML}
    fi
else
    curl -c ${myCookie} https://${URLROOT} -o ${oneHTML} 2>/dev/null
fi

#native code decoder
s0="(0)"
o0="0"
s1="(1)"
o1="1"
s2="(1+1)"
o2="2"
s3="(1+1+1)"
o3="3"
s4="(1+1+1+1)"
o4="4"
s5="(1+1+1+1+1)"
o5="5"
s6="(1+1+1+1+1+1)"
o6="6"
s7="(1+1+1+1+1+1+1)"
o7="7"
s8="(1+1+1+1+1+1+1+1)"
o8="8"
s9="(1+1+1+1+1+1+1+1+1)"
o9="9"


#Check if anti-DDoS
# "cf-browser-verification cf-im-under-attack"
if $(grep "cf-browser-verification cf-im-under-attack" ${oneHTML} >/dev/null) ; then
    echo "Anti-DDoD activated..." >&2
    OLDIFS=${IFS}
    IFS=
    #in TMP1 we get the script
    getPart "script" ${oneHTML} >${tmp1}
    #in TMP2 the from to send
    getPart "form" ${oneHTML} >${tmp2}
    IFS=${OLDIFS}


    #In the form, we have 9 variables.
    formID=
    action=
    method=

    i1_name=
    i1_val=

    i2_name=
    i2_val=

    i3_name=
    i3_ID=


    #For all lines of the form :
    while IFS= read line; do
        #echo try line ${line}
        #The first line of <form> contains the "method" string.
        if $(echo ${line} | grep "method" >/dev/null) ; then
            formID=$(echo ${line} | sed 's/.*id="\([-[:alnum:]]*\)".*/\1/g')
            action=$(echo ${line} | sed 's/.*action="\([-/_[:alnum:]]*\)".*/\1/g')
            method=$(echo ${line} | sed 's/.*method="\([-[:alnum:]]*\)".*/\1/g')
        #As it is a elif, we will never see again other "id" execpt the fourth line.
        elif $(echo ${line} | grep "id=" >/dev/null) ; then
            i3_name=$(echo ${line} | sed 's/.*name="\([-_[:alnum:]]*\)".*/\1/g')
            i3_ID=$(echo ${line} | sed 's/.*id="\([-[:alnum:]]*\)".*/\1/g')
        #Remains the two similar inputs, with same keys. But one contains an underscore, keep the elif in mind
        elif $(echo ${line} | grep "_" >/dev/null) ; then
            i1_name=$(echo ${line} | sed 's/.*name="\([-_[:alnum:]]*\)".*/\1/g')
            i1_val=$(echo ${line} | sed 's/.*value="\([-\./_[:alnum:]]*\)".*/\1/g')
        #Remains the last one, but also </form>, then we have just to treat form first.
        elif $(echo ${line} | grep "form" >/dev/null) ; then
            doNothing=
        #Or we also could look for "input".
        else # $(echo ${line} | grep "input" >/dev/null) ; then
            i2_name=$(echo ${line} | sed 's/.*name="\([-_[:alnum:]]*\)".*/\1/g')
            i2_val=$(echo ${line} | sed 's/.*value="\([-\./_[:alnum:]]*\)".*/\1/g')
        fi
    done < ${tmp2}

    #Check the values
    # echo -e "\n"
    # echo -e "Form ID\t${formID}\taction\t${action}\t\tmethod\t${method}"
    # echo -e "i1_name\t${i1_name}\ti1_val\t${i1_val}"
    # echo -e "i2_name\t${i2_name}\ti2_val\t${i2_val}"
    # echo -e "i3_ID\t${i3_ID}\ti3_name\t${i3_name}"


    timeOut=1000
    suffix=

    #For all lines of the script :
    while IFS= read line; do
        #echo try line ${line}
        #There is no much interessant in this javascript code...
        #Only recovering the time before submitting the form, and maybe add the # behind action.
        if $(echo ${line} | grep -E "}, ?[[:digit:]]+" >/dev/null) ; then
            timeOut=$(echo ${line} | sed 's/.* \([[:digit:]]*\).*/\1/g')
        #If we have a suffix
        elif $(echo ${line} | grep "action" >/dev/null) ; then
            #Yes ! A double check !
            if $(echo ${line} | grep "location.hash" >/dev/null) ; then
                suffix='#'
            fi
        #Zut... It is not only that, we need to compute the thing...
        #Here the first part
        elif $(echo ${line} | grep "s,t,o,p,b,r,e,a,k,i,n,g" >/dev/null) ; then
            #Need to compute...
            echo "PARTIE 1" >&2
            tmp=$(echo ${line} | sed -e "s/^.*={\(.*\).*};$/\1/" | \
                            sed -e "s/\!/#/g" -e "s/\[\]/c/g" | sed -e "s/##c/1/g" -e 's/#+c/1/g' | sed -e 's/c/0/g' | sed -e 's/(+/(/g' | sed -e 's/+0//g' )
            #echo ${tmp}
            part1=$(echo ${tmp} |   sed -e "s/$s0/$o0/g" | \
                                    sed -e "s/$s1/$o1/g" | \
                                    sed -e "s/$s2/$o2/g" | \
                                    sed -e "s/$s3/$o3/g" | \
                                    sed -e "s/$s4/$o4/g" | \
                                    sed -e "s/$s5/$o5/g" | \
                                    sed -e "s/$s6/$o6/g" | \
                                    sed -e "s/$s7/$o7/g" | \
                                    sed -e "s/$s8/$o8/g" | \
                                    sed -e "s/$s9/$o9/g" | sed -e "s/+\([[:digit:]]\)/\1/g" | \
                                    sed -e "s/:+/:/g"    | sed -e "s/\/+/\//g" )
            unset tmp
            #echo ${part1}
            alias=$(echo ${part1} |   sed -e "s/.*\"\([[:alnum:]]*\)\":.*/\1/g" )
            base=$(echo ${part1}  |   sed -e "s/.*${alias}\":\(.*\)/\1/g" )
            base=$(echo "scale=${precisionFine};${base}" | bc)
            echo -e "Alias:${alias} \t BaseSum:${base}"
        #second part
        elif $(echo ${line} | grep "\.toFixed" >/dev/null) ; then
            echo "PARTIE 2" >&2
            tmp=$(echo ${line} |  sed -e "s/^.*={\(.*\).*};$/\1/" | \
                                sed -e "s/\!/#/g" -e "s/\[\]/c/g" | sed -e "s/##c/1/g" -e 's/#+c/1/g' | sed -e 's/c/0/g' | sed -e 's/(+/(/g' | sed -e 's/+0//g' )
            #echo ${tmp}
            part2=$(echo ${tmp} |   sed -e "s/$s0/$o0/g" | \
                                    sed -e "s/$s1/$o1/g" | \
                                    sed -e "s/$s2/$o2/g" | \
                                    sed -e "s/$s3/$o3/g" | \
                                    sed -e "s/$s4/$o4/g" | \
                                    sed -e "s/$s5/$o5/g" | \
                                    sed -e "s/$s6/$o6/g" | \
                                    sed -e "s/$s7/$o7/g" | \
                                    sed -e "s/$s8/$o8/g" | \
                                    sed -e "s/$s9/$o9/g" | sed -e "s/+\([[:digit:]]\)/\1/g")
            unset tmp
            unset sum
            calculus=$(echo ${base})
            #echo ${part2}
            
            #echo ${calculus}
            for i in $(echo ${part2} | sed "s/;/\n/g"); do
                if $(echo ${i} | grep ".toFixed" >/dev/null) ; then
                    precision=$(echo ${i} | sed "s/.*toFixed(\([[:digit:]]*\)).*/\1/")
                    decimal=$(echo ${calculus} | cut -d"." -f2)
                    integer=$(echo ${calculus} | cut -d"." -f1)
                    calculus=$(echo "${integer}.${decimal:0:$precision}")
                    URL_Size=11;
                    calculus=$(echo "scale=${precisionFine};${calculus}+${URL_Size}" | bc)
                    echo ${calculus} >&2
                elif $(echo ${i} | grep "${alias}" >/dev/null) ; then
                    i=$(echo "${i}" | sed -e "s/\(.*${alias}\)\([-+\*\/]\)\(=+\)\(.*\)/\2=\4/" -e "s/\/+/\//g")
                    #$(echo ${i} | cut -c1)
                    case $(echo ${i} | cut -c1) in
                        '-')
                            tmp=$(echo "scale=${precisionFine};$(echo ${i} | cut -c3-)" | bc)
                            calculus=$(echo "scale=${precisionFine};${calculus}-${tmp}" | bc)
                            #echo ${calculus}
                        ;;
                        '+')
                            tmp=$(echo "scale=${precisionFine};$(echo ${i} | cut -c3-)" | bc)
                            calculus=$(echo "scale=${precisionFine};${calculus}+${tmp}" | bc)
                            #echo ${calculus}
                        ;;
                        '*')
                            tmp=$(echo "scale=${precisionFine};$(echo ${i} | cut -c3-)" | bc)
                            calculus=$(echo "scale=${precisionFine};${calculus}*${tmp}" | bc)
                            #echo ${calculus}
                        ;;
                        '/')
                            tmp=$(echo "scale=${precisionFine};$(echo ${i} | cut -c3-)" | bc)
                            calculus=$(echo "scale=${precisionFine};${calculus}/${tmp}" | bc)
                            #echo ${calculus}
                        ;;
                        *)
                        ;;
                    esac
                    
                fi
            done
            i3_ID=${calculus}
        #We do nothing for all other lines.
        #else
        #    echo
        fi
    done < ${tmp1}


    sleep ${timeOut}e-03


    if [ 1 -eq "${dryRun}" ]; then
        echo "curl -c ${myCookie} \"http://${URLROOT}${action}${suffix}${i1_name}=${i1_val}&${i2_name}=${i2_val}&${i3_name}=${i3_ID}\" -o ${oneHTML} 2>/dev/null" >&2
    else
        curl -b ${myCookie} "http://${URLROOT}${action}${suffix}${i1_name}=${i1_val}&${i2_name}=${i2_val}&${i3_name}=${i3_ID}" -o ${oneHTML} 2>/dev/null
    fi
else
    echo "Anti-DDoS system not enabled." >&2
fi

#echo "Get ID and informations on the leetchi fundrasing page." >&2
#echo "curl -b ${myCookie} "https://${urlbase}/${rest}" -o ${oneHTML} #2>/dev/null"
curl -b ${myCookie} "https://${urlbase}/${rest}" -o ${oneHTML} 2>/dev/null

#Get the fundraising ID
FundraisingId=$( grep "__FundraisingId" ${oneHTML} | sed "s/^[[:space:]]*var __FundraisingId = \"\([[:alnum:]]*\)\".*/\1/" )

#            'NbParticipations': '51427',
nbParticipant=$(grep "'NbParticipations':" ${oneHTML} | sed "s/^[[:space:]]*'NbParticipations': '\([[:digit:]]*\)'.*/\1/" )
#Get how much the API give participants
nbParticipantPerAsk=$( grep "__MaxParticipationToShow" ${oneHTML} | sed "s/^[[:space:]]*var __MaxParticipationToShow = \"\([[:digit:]]*\)\".*/\1/" )

#get the number of messages posted
nbMessages=$( grep "fdr-messages-more" ${oneHTML} | sed "s/.*data-limit=\"\([[:digit:]]*\)\".*/\1/" )
nbMessagesPerAsk=10

case ${get} in
    "informations")
        FR_name=$(grep -A 1 "h1 class=\"page-heading\"" ${oneHTML} | sed "/<h1/d" | sed "s/^[[:space:]]*//" | sed "s/<\/*span>//g" )
        FR_ID__=$(echo ${FundraisingId})
        FR_descTmp=$(grep -A 1 "class=\"fdr-description\"" ${oneHTML} | sed "/class=\"fdr-description\"/d" | sed "s/^[[:space:]]*//" | sed "s/ //g" | sed "s/<p>//g" | sed "s/<\/?strong>/**/g" | sed "s/<\/p>/\/CRLF/g" )
        FR_desc=$(echo ${FR_descTmp} | sed -e "s/<h3>//" -e "s/<\/h3>/\/CRLF/g" -e "s/<ul>//g" -e "s/<\/ul>/\/CRLF/g" -e "s/<\/\?li>//g" -e "s/<\/\?u>//g" )
        FR_SUM_=$(grep "'CollectedAmount':" ${oneHTML} | sed "s/[[:space:]]*'CollectedAmount': '\([[:digit:]]*\)'.*/\1/" | sed "s/\([[:digit:]]*\)\([[:digit:]][[:digit:]]\)$/\1,\2/")
        FR_Contribs=$(echo ${nbParticipant})
        FR_Comments=$(echo ${nbMessages})
        FR_Closing=$(grep -A 1 "c-status__column c-delay" ${oneHTML} | sed "/c-delay/d" | sed "/--/d" | uniq -d | sed "s/<\/\?span[-=\"_[:space:][:alnum:]]*>//g" )
        FR_Opener=$(grep "class=\"fundraiser-firstname\"" ${oneHTML} | uniq -d | sed "s/<\/\?span[-=\"_[:space:][:alnum:]]*>//g" )
        FR_beneficiary=$(grep "class=\"fdr-beneficiary\"" ${oneHTML} | sed "s/^<span class=\"fdr-beneficiary\">//" | sed "s/<\/span>//" )
        echo "" >&2
        echo -e "Name\t\t: ${FR_name}"
        echo -e "ID\t\t: ${FR_ID__}"
        echo -e "Description\t: ${FR_desc}"
        echo -e "Collected\t: ${FR_SUM_}"
        echo -e "Contributors\t: ${FR_Contribs}"
        echo -e "Comments\t: ${FR_Comments}"
        echo -e "Remaining time\t: ${FR_Closing}"
        echo -e "Fundraiser\t: ${FR_Opener}"
        echo -e "Beneficiary\t: ${FR_beneficiary}"
        ;;
    "donors")
        i=0
        echo "Downloading donor list." >&2
        while [ ${i} -lt ${nbParticipant} ]; do
            echo -e "\r${i}/${nbParticipant}\c" >&2
            curl -b ${myCookie} "https://${urlbase}/fr/Fundraising/PartialBlocParticipationsList/?fundraisingId=${FundraisingId}&onlyPublic=True&skip=${i}" 2>/dev/null 1>>${output}
            i=$(echo "${i}+${nbParticipantPerAsk}" | bc)
        done
        ;;
    "comments")
        i=0
        echo "Downloading comments list" >&2
        while [ ${i} -lt ${nbMessages} ]; do
            echo -e "\r${i}/${nbMessages}\c" >&2
            curl -b ${myCookie} "https://${urlbase}/fr/Fundraising/PartialBlocMessagesList/?fundraisingId=${FundraisingId}&onlyPublic=True&skip=${i}" 2>/dev/null 1>>${output}
            i=$(echo "${i}+${nbMessagesPerAsk}" | bc)
        done
        ;;
    *)
        ;;
esac
echo ""

#Wait for any input
#echo "Press any key to quit."
#read -s -n 1 C

#echo "Remove temporary files."
rm -f ${myCookie} ${tmp1} ${tmp2} ${oneHTML} 
