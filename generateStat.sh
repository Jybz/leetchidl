#!/bin/sh
clear
clear

################################
#      FUNCTIONS PART          #
################################

PrintHelp()
{
    echo -e "${0} --input=[...] --output_prefix=[...]" >&2
    echo -e "--input=[...]\t: the input file to analyse." >&2
    echo -e "--output_prefix=[...]\t: the prefix for output files." >&2
    echo -e "--help\t: this help." >&2
}

################################
#      START CODE PART         #
################################

#If no arguments, print help
if [ $# == 0 ]; then
    PrintHelp
    exit
fi

hasInput=0
hasOutputP=0
hasHelp=0

echo "Check arguments" >&2
for args in ${*}; do
    
    ## Check if query for help
    if [ ${args} == "--help" ]; then
        hasHelp=1
    ##Check all options
    #case --input=
    elif [ 1 -eq $( echo "${args}" | grep -c "input=") ] ; then
        inFile=$( echo "${args}" | sed -e "s/^\(--input=\)\(.*\)$/\2/" )
        if [ -e ${inFile} ]; then
            if [ -d ${inFile} ]; then
                echo "The input is a directory !" >&2
                hasInput=0
            elif [ -f ${outFile} ]; then
                hasInput=1
            fi
        else
            echo "Input file doesn't exist !" >&2
            hasInput=O
        fi
    #case --output_prefix=
    elif [ 1 -eq $( echo "${args}" | grep -c "output_prefix=") ] ; then
        outPrefix=$( echo "${args}" | sed -e "s/^\(--output_prefix=\)\(.*\)$/\2/" )
        hasOutputP=1
    fi
done

if [ 1 -eq ${hasHelp} ]; then
    PrintHelp
    exit
elif [ 0 -eq ${hasOutputP} ]; then
    echo "Need output file prefix." >&2
    exit
elif [ 0 -eq ${hasInput} ]; then
    echo "Need input file to analyse." >&2
    exit
fi


#make some temporary variable which will remains in the RAM.
tmp1=$(mktemp)
tmp2=$(mktemp)

#Copie input file in memory
cat ${inFile} > ${tmp1}
type=$(head -n 1 ${tmp1} | sed -e "s/[[:space:]*]<div class=\"//" -e "s/\">//")


case ${type} in
    "fdr-contributor")
        echo "Proceed stat on contributors." >&2
        #delete one line
        sed -i "/class=\"fdr-contributor\"/d" ${tmp1}
        sed -i "/class=\"fdr-contributor-picture\"/d" ${tmp1}
        sed -i "/class=\"fdr-contributor-club club-vip\"/d" ${tmp1}
        
        #Clean
        cat ${tmp1} | sed -e "/class=\"fdr-contributor-detail\"/d" | sed -e "/a particip&#233;/d" | sed -e "/^<\/div>/d" | sed -e "s/<\/p>//" | sed -e "s/<\/span>//" | sed -e "s/ class=\"fdr-contributor-name\"//" >${tmp2}
        cat ${tmp2} | sed -e "s/<span class=\"fdr-contributor-contribution\">/GIVE:/" >${tmp1}
        cat ${tmp1} | sed -e "s/<span title=\"[-\/&#;\.[:space:][:alnum:]]*\">/NAME:/" >${tmp2}
        
        
        #Change some chars
        sed -i \
        -e "s/&#39;/\'/g" \
        -e "s/&#233;/é/g" \
        -e "s/&#224;/à/g" \
        -e "s/&#234;/ê/g" \
        -e "s/&#232;/è/g" \
        -e "s/&#226;/â/g" \
        -e "s/&#231;/ç/g" \
        -e "s/&#239;/ï/g" \
        -e "s/&#235;/ë/g" \
        -e "s/&#244;/ô/g" \
        -e "s/&quot;/\"/g" \
        ${tmp2}
        
        
        #Extract the list of names
        grep "^NAME:" ${tmp2} | sed -e "s/^NAME://" | sort | uniq -c | sort -bnr >${outPrefix}.contributors.1.names
        #save list
        cat ${tmp2} >${outPrefix}.contributors.0.rawList
        
        #Give informations :
        echo -e "\tFull extract list\t: ${outPrefix}.contributors.0.rawList" >&2
        echo -e "\tList of names\t\t: ${outPrefix}.contributors.1.names" >&2
        echo -e "Special command, it gives you the amount given for associated name:\n\t$ grep -A 1 \"NAME:one name\" ./${outPrefix}.contributors.0.rawList" >&2
        echo -e " " >&2
        ;;
        
    "fdr-message")
        echo "Proceed stat on messages." >&2
        #change multiple line message to one-line message
        #delete profile picture.
        cat ${tmp1} | tr '\n' '`' | sed "s/<\/div>\`/\n/g" | sed -e "/fdr-message-author/d" | sed "s/^[[:space:]]*//" >${tmp2}
        #Change date markers
        cat ${tmp2} | sed -e "s/<div class=\"fdr-message\">\`<div class=\"fdr-message-date\">/DATE:/" >${tmp1}
        #Change message markers
        cat ${tmp1} | sed -e "s/<div class=\"fdr-message-message\">/MESS:/" >${tmp2}
        #Change name markers
        cat ${tmp2} | sed -e "s/<div class=\"fdr-message-name\">/NAME:/" >${tmp1}
        
        #Change some chars
        sed -i \
        -e "s/&#39;/\'/g" \
        -e "s/&#233;/é/g" \
        -e "s/&#224;/à/g" \
        -e "s/&#234;/ê/g" \
        -e "s/&#232;/è/g" \
        -e "s/&#226;/â/g" \
        -e "s/&#231;/ç/g" \
        -e "s/&#239;/ï/g" \
        -e "s/&#235;/ë/g" \
        -e "s/&#244;/ô/g" \
        -e "s/&quot;/\"/g" \
        ${tmp1}
        
        #Delete space between name and message
        sed -i "/NAME:/{n;d}" ${tmp1}
        
        #change date format
        sed -i -e "/DATE:/ s/:Le /:/" ${tmp1}
        sed -i -e "/DATE:/ s/janvier/01/" ${tmp1}
        sed -i -e "/DATE:/ s/février/02/" ${tmp1}
        sed -i -e "/DATE:/ s/mars/03/" ${tmp1}
        sed -i -e "/DATE:/ s/avril/04/" ${tmp1}
        sed -i -e "/DATE:/ s/mai/05/" ${tmp1}
        sed -i -e "/DATE:/ s/juin/06/" ${tmp1}
        sed -i -e "/DATE:/ s/juillet/07/" ${tmp1}
        sed -i -e "/DATE:/ s/août/08/" ${tmp1}
        sed -i -e "/DATE:/ s/septembre/09/" ${tmp1}
        sed -i -e "/DATE:/ s/octobre/10/" ${tmp1}
        sed -i -e "/DATE:/ s/novembre/11/" ${tmp1}
        sed -i -e "/DATE:/ s/décembre/12/" ${tmp1}
        sed -i -e "/DATE:/ s/\([[:digit:]]*\) \([[:digit:]]*\) \([[:digit:]]*\)/\3_\2_\1/" ${tmp1}

        #Change ` by /CRLF
        sed -i -e "s/\`/\/CRLF /g" ${tmp1}
        
        #Extract the list of names
        grep "^NAME:" ${tmp1} | sed -e "s/^NAME://" | sort | uniq -c | sort -bnr >${outPrefix}.messages.1.names
        
        #Extract the list of comments
        grep "^MESS:" ${tmp1} | sed -e "s/^MESS://" | sort | uniq -c | sort -bnr >${outPrefix}.messages.2.comments
        
        #save list
        cat ${tmp1} >${outPrefix}.messages.0.rawList
        
        #Give informations :
        echo -e "\tFull extract list\t: ${outPrefix}.messages.0.rawList" >&2
        echo -e "\tList of names\t\t: ${outPrefix}.messages.1.names" >&2
        echo -e "\tList of comments\t: ${outPrefix}.messages.2.comments" >&2
        echo -e "Special command, it gives you the messages for associated name:\n\t$ grep -A 1 \"NAME:one name\" ./${outPrefix}.messages.0.rawList" >&2
        echo -e " " >&2
        ;;
        
    *)
        ;;
esac

#echo "Remove temporary files."
rm -f ${tmp1} ${tmp2} 
